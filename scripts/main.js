const secondHand = document.querySelector('.second-hand');
const minuteHand = document.querySelector('.minute-hand');
const hourHand = document.querySelector('.hour-hand');
const time = document.querySelector('.time');
const body = document.getElementsByTagName('body')[0];

// toma los segundos, minutos y horas actuales
// calcula los grados a rotar en cada aguja del reloj (div con class hand)
// aplica la rotacion correspondiente a cada div
function setDate() {
	const now = new Date();
	
	const seconds = now.getSeconds();
	const secondsDegrees = (seconds / 60 * 360) + 90;
	secondHand.style.transform = `rotate(${secondsDegrees}deg)`;
	
	const minutes = now.getMinutes();
	const minutesDegrees = (minutes / 60 * 360) + 90;
	minuteHand.style.transform = `rotate(${minutesDegrees}deg)`;
	
	const hours = now.getHours();
	const hoursDegrees = (hours / 12 * 360) + 90;
	hourHand.style.transform = `rotate(${hoursDegrees}deg)`;

	time.textContent = `${hours}:${minutes}:${seconds}`
	
}

function changeBack(){

	if (body.classList.contains('background1')){
		body.className = 'background2';
	}
	else if (body.classList.contains('background2')){
		body.className = 'background3';
	}
	else {
		body.className = 'background1';
	}

}

// Llama a la funcion setDate cada 1 segundo (1000 milisegundos)
setInterval(setDate, 1000);
setInterval(changeBack, 5000);